from django.shortcuts import render
from django.http import HttpResponse, HttpResponsePermanentRedirect
from api.models import Location
from api.serializers import LocationSerializers
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from django.http import JsonResponse
from api.jsonmaps import array

import json
import requests
import sys
import cgi



def index(request):
	# addresses = Location.objects.all()
	# print(addresses)
	# return render(request, "index.html", {"addresses": addresses})
	return render(request, "index.html")

def coord_in(request):
	for x in array:
		try:
			Location.objects.get(name=x['name'])
		except Housing.DoesNotExist:
			YandexMapsUrl = "https://geocode-maps.yandex.ru/1.x/?apikey=82a32a7f-84a2-4eb0-bd73-fe49223472c7&format=json&geocode={}".format(x['adress'])
			req = requests.get(YandexMapsUrl)
			res = req.json()
			result = res['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']
			result = result.split(' ')
			location_l = result[0]
			location_u = result[1]
			print(result)
			oper = Location.objects.create(
											name=x['name'], adress=x['adress'],
											location_l=location_l, location_u=location_u
											)
			oper.save()

	return HttpResponsePermanentRedirect('coordout')

def json(request):
	storage = cgi.FieldStorage()
	data = storage.getvalue('data')
	addresses = Location.objects.filter(count=0).values()
	return JsonResponse({'body': list(addresses)})


class Coordout(APIView):
	def get(self, reqests):
		addresses = Location.objects.all()
		serializers = LocationSerializers(addresses, many=True)
		return Response({"data": serializers.data})
