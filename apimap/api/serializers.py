from rest_framework import serializers
from api.models import Location

class LocationSerializers(serializers.ModelSerializer):
	class Meta:
		model = Location
		fields = ('name', 'adress', 'location_l', 'location_u')