from django.db import models

class Location(models.Model):
    name = models.CharField("Название", max_length=300, default=False)
    adress = models.CharField("Адрес", max_length=300, default=False)
    location_l = models.CharField("Долгота", max_length=300, default=False)
    location_u = models.CharField("Широта", max_length=300, default=False)
    date = models.DateTimeField("Дата создания", auto_now_add=True)

    count = models.IntegerField("Порядок", default=0)


