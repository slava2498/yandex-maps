from api.models import Location
from api.serializers import LocationSerializers
import cgi
from django.http import JsonResponse

storage = cgi.FieldStorage()
data = storage.getvalue('data')
if data is not None:
    addresses = Location.objects.all()
    serializer = LocationSerializers(addresses)
    content = JSONRenderer().render(serializer.data)
    return JsonResponse({'data': content, 'state': True})
return JsonResponse({'state': False})