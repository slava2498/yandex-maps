ymaps.ready(function () {
    $.ajax({
        type: "GET",
        url: '/json',
        data: "hi",
        success: function(msg){
            coord = msg['body'];

            var myMap = new ymaps.Map('map', {
                    center: [53.757547, 87.136044],
                    zoom: 16
                }, {
                    searchControlProvider: 'yandex#search'
                }),

                // Создаём макет содержимого.
                MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                    '<div style="color: #000000; font-weight: bold;">$[properties.iconContent]</div>'
                )
            
            for (var i = 0, l = coord.length; i < l; i++) {
                console.log(coord[i])
                myPlacemarkWithContent = new ymaps.Placemark([coord[i]['location_u'], coord[i]['location_l']], {
                    hintContent: 'Соборы',
                    balloonContent: coord[i]['name'],
                    iconContent: coord[i]['count']
                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#imageWithContent',
                    // Своё изображение иконки метки.
                    iconImageHref: src,
                    // Размеры метки.
                    iconImageSize: [48, 48],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-24, -24],
                    // Смещение слоя с содержимым относительно слоя с картинкой.
                    iconContentOffset: [15, 15],
                    // Макет содержимого.
                    iconContentLayout: MyIconContentLayout
                });
            }   
            var multiRoute = new ymaps.multiRouter.MultiRoute({
            // Описание опорных точек мультимаршрута.
            referencePoints: [
                coord[0]['adress'],
                coord[1]['adress'],
                coord[2]['adress'],
                coord[3]['adress'],
            ],
            // Параметры маршрутизации.
            params: {
                // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                results: 2
            }
            }, {
                // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                boundsAutoApply: true
            });
            var referencePoints = multiRoute.model.getReferencePoints();
            referencePoints.splice(1, 0,  coord[2]['adress']);
            /**
             * Добавляем транзитную точку в модель мультимаршрута.
             * Обратите внимание, что транзитные точки могут находится только
             * между двумя путевыми точками, т.е. не могут быть крайними точками маршрута.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRouteModel.xml#setReferencePoints
             */
            multiRoute.model.setReferencePoints(referencePoints, [1]);
            // Добавляем мультимаршрут на карту.
            myMap.geoObjects.add(multiRoute);    
            myMap.geoObjects.add(myPlacemarkWithContent);
            
            
            
            
            
            

            
        }
    });
});


            // // Создаем кнопки для управления мультимаршрутом.
            // var trafficButton = new ymaps.control.Button({
            //         data: { content: "Учитывать пробки" },
            //         options: { selectOnClick: true }
            //     }),
            //     viaPointButton = new ymaps.control.Button({
            //         data: { content: "Добавить транзитную точку" },
            //         options: { selectOnClick: true }
            //     });
        
            // // Объявляем обработчики для кнопок.
            // trafficButton.events.add('select', function () {
            //     /**
            //      * Задаем параметры маршрутизации для модели мультимаршрута.
            //      * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRouteModel.xml#setParams
            //      */
            //     multiRoute.model.setParams({ avoidTrafficJams: true }, true);
            // });
        
            // trafficButton.events.add('deselect', function () {
            //     multiRoute.model.setParams({ avoidTrafficJams: false }, true);
            // });
        
            // viaPointButton.events.add('select', function () {
            //     var referencePoints = multiRoute.model.getReferencePoints();
            //     referencePoints.splice(1, 0, "Москва, ул. Солянка, 7");
            //     /**
            //      * Добавляем транзитную точку в модель мультимаршрута.
            //      * Обратите внимание, что транзитные точки могут находится только
            //      * между двумя путевыми точками, т.е. не могут быть крайними точками маршрута.
            //      * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRouteModel.xml#setReferencePoints
            //      */
            //     multiRoute.model.setReferencePoints(referencePoints, [1]);
            // });
        
            // viaPointButton.events.add('deselect', function () {
            //     var referencePoints = multiRoute.model.getReferencePoints();
            //     referencePoints.splice(1, 1);
            //     multiRoute.model.setReferencePoints(referencePoints, []);
            // });
        
            // // Создаем карту с добавленными на нее кнопками.
            // var myMap = new ymaps.Map('map', {
            //     center: [55.750625, 37.626],
            //     zoom: 7,
            //     controls: [trafficButton, viaPointButton]
            // }, {
            //     buttonMaxWidth: 300
            // });
        
            // // Добавляем мультимаршрут на карту.
            // myMap.geoObjects.add(multiRoute);







