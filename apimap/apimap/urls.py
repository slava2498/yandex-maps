from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from api import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.index),
    url(r'^coordin/', views.coord_in),
    url(r'^coordout/', views.Coordout.as_view()),
    url(r'^json/', views.json),
]

